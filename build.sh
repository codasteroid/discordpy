#!/usr/bin/env bash

# exit an error
set -o errexit

# install dependencies
pip install -r requirements.txt

