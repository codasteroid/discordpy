import discord
import os
import random
from discord.ext import commands, tasks
from dotenv import load_dotenv
from itertools import cycle


load_dotenv()

intents = discord.Intents.all()
# client = discord.Client(intents=intents)
client = commands.Bot(command_prefix='./', intents=intents)


# Define activities
games = cycle(['Discord.py', 'Nothing'])
status = cycle(['online', 'idle'])


@client.event
async def on_ready():
    # await client.change_presence(status=discord.Status.online, activity=discord.Game('Discord.py'))
    change_status.start()
    print('driedberry is ready ...')


@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send('You do not have permission to use this command.')


@tasks.loop(seconds=7)
async def change_status():
    await client.change_presence(status=discord.Status(next(status)), activity=discord.Game(next(games)))


@client.event
async def on_member_join(member):
    print(f'{member} has joined the server.')
    guild = member.guild
    if guild.system_channel is not None:
        message = f'Welcome {member.mention} to {guild.name}!'
        await guild.system_channel.send(message)


@client.event
async def on_member_remove(member):
    print(f'{member} has left the server.')
    guild = member.guild
    if guild.system_channel is not None:
        message = f'{member.mention} left {guild.name}!'
        await guild.system_channel.send(message)


@client.command()
@commands.has_role('Contributor')
async def ping(ctx):
    await ctx.send('Pong!')


@client.command(aliases=['8ball', 'test'])
async def _8ball(ctx, *, question):
    responses = [
        'It is certain.',
        'Yes, definetly',
        'As I see it, yes',
        'My sources say no',
        'Yes'
    ]
    await ctx.send(f'Question: {question}\nAnswer: {random.choice(responses)}')


@client.command(aliases=['delete', 'remove'])
@commands.has_permissions(manage_messages=True)
async def clear(ctx, amount=3):
    await ctx.channel.purge(limit=amount)


@client.command()
async def kick(ctx, member: discord.Member, *, reason=None):
    await member.kick(reason=reason)


@client.command()
async def ban(ctx, member: discord.Member, *, reason=None):
    await member.ban(reason=reason)
    await ctx.send(f'{member.mention} is banned.')


@client.command()
async def unban(ctx, *, member: discord.Member):
    await ctx.guild.unban(member)
    await ctx.send(f'{member.mention} is unbanned.')

# @client.command()
# async def unban(ctx, *, member):
#     banned_users = await ctx.guild.bans()

#     for ban_entry in banned_users:
#         user = ban_entry.user
#         if user == member:
#             await ctx.guild.unban(user)
#             await ctx.send(f'{user.mention} has been unbanned!')
#             return
#     await ctx.send(f'{member} is not currently banned from this server.')

if __name__ == '__main__':
    client.run(os.getenv('DISCORD_TOKEN'))
